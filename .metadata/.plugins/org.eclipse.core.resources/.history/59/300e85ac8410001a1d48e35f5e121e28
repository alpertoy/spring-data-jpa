package io.gitlab.alpertoy.springdata.associations;


import java.util.Set;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.associations.onetomany.entities.Customer;
import io.gitlab.alpertoy.springdata.associations.onetomany.entities.PhoneNumber;
import io.gitlab.alpertoy.springdata.associations.onetomany.repos.CustomerRepository;

@SpringBootTest
class AssociationsApplicationTests {
	
	@Autowired
	CustomerRepository repository;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void testCreateCustomer() {
		
		Customer customer = new Customer();
		customer.setName("Alper");
		
		
		PhoneNumber ph1 = new PhoneNumber();
		ph1.setNumber("1234567890");
		ph1.setType("cell");

		
		PhoneNumber ph2 = new PhoneNumber();
		ph2.setNumber("0987654321");
		ph2.setType("home");


		customer.addPhoneNumber(ph1);
		customer.addPhoneNumber(ph2);
		repository.save(customer);
	}
	
	@Test
	//Transactional is required for Fetch type Lazy loading as default
	@Transactional
	public void testLoadCustomer() {
		Customer customer = repository.findById(11L).get();
		System.out.println(customer.getName());
		
		Set<PhoneNumber> numbers = customer.getNumbers();
		numbers.forEach(number -> System.out.println(number.getNumber()));
	}
	
	@Test
	public void testUpdateCustomer() {
		Customer customer = repository.findById(11L).get();
		customer.setName("Alper Toy");
		
		Set<PhoneNumber> numbers = customer.getNumbers();
		numbers.forEach(number -> number.setType("cell"));
		
		repository.save(customer);
	}

}
