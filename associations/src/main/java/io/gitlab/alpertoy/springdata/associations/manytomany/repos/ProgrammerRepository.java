package io.gitlab.alpertoy.springdata.associations.manytomany.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.associations.manytomany.entities.Programmer;

public interface ProgrammerRepository extends CrudRepository<Programmer, Integer> {

}
