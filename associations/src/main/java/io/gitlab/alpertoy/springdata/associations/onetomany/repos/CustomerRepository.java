package io.gitlab.alpertoy.springdata.associations.onetomany.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.associations.onetomany.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
