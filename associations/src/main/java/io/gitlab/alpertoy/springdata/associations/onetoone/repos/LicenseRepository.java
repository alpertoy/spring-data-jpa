package io.gitlab.alpertoy.springdata.associations.onetoone.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.associations.onetoone.entities.License;

public interface LicenseRepository extends CrudRepository<License, Long> {

}
