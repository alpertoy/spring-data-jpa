package io.gitlab.alpertoy.springdata.associations;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.associations.manytomany.entities.Programmer;
import io.gitlab.alpertoy.springdata.associations.manytomany.entities.Project;
import io.gitlab.alpertoy.springdata.associations.manytomany.repos.ProgrammerRepository;
import io.gitlab.alpertoy.springdata.associations.onetomany.entities.Customer;
import io.gitlab.alpertoy.springdata.associations.onetomany.entities.PhoneNumber;
import io.gitlab.alpertoy.springdata.associations.onetomany.repos.CustomerRepository;
import io.gitlab.alpertoy.springdata.associations.onetoone.entities.License;
import io.gitlab.alpertoy.springdata.associations.onetoone.entities.Person;
import io.gitlab.alpertoy.springdata.associations.onetoone.repos.LicenseRepository;

@SpringBootTest
class AssociationsApplicationTests {
	
	@Autowired
	CustomerRepository repository;
	
	@Autowired
	ProgrammerRepository programmerRepository;
	
	@Autowired
	LicenseRepository licenseRepository;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void testCreateCustomer() {
		
		Customer customer = new Customer();
		customer.setName("Alper");
		
		
		PhoneNumber ph1 = new PhoneNumber();
		ph1.setNumber("1234567890");
		ph1.setType("cell");

		
		PhoneNumber ph2 = new PhoneNumber();
		ph2.setNumber("0987654321");
		ph2.setType("home");


		customer.addPhoneNumber(ph1);
		customer.addPhoneNumber(ph2);
		repository.save(customer);
	}
	
	@Test
	// @Transactional is required for using Fetchtype.LAZY that is loading as default
	// It is used for loading data from child class as well in more than one query!
	// For better performance
	@Transactional
	public void testLoadCustomer() {
		Customer customer = repository.findById(11L).get();
		System.out.println(customer.getName());
		
		Set<PhoneNumber> numbers = customer.getNumbers();
		numbers.forEach(number -> System.out.println(number.getNumber()));
	}
	
	@Test
	public void testUpdateCustomer() {
		Customer customer = repository.findById(11L).get();
		customer.setName("Alper Toy");
		
		Set<PhoneNumber> numbers = customer.getNumbers();
		numbers.forEach(number -> number.setType("cell"));
		
		repository.save(customer);
	}
	
	@Test
	public void testDelete() {
		repository.deleteById(11L);
	}
	
	//ManyToMany tests
	
	@Test
	public void testmtomCreateProgrammer() {
		Programmer programmer = new Programmer();
		programmer.setName("John");
		programmer.setSalary(1000);
		
		HashSet<Project> projects = new HashSet<Project>();
		
		Project project = new Project();
		project.setName("Hibernate Project");
		projects.add(project);
		programmer.setProjects(projects);
		
		programmerRepository.save(programmer);
		
	}
	
	@Test
	@Transactional
	// @Transactional is required for using Fetchtype.LAZY that is loading as default
	// Otherwise we get exception
	// It is used for loading data from child class as well in more than one query!
	// For better performance
	public void testmtomFindProgrammer() {
		Programmer programmer = programmerRepository.findById(16).get();
		System.out.println(programmer);
		System.out.println(programmer.getProjects());
	}
	
	//OneToOne tests
	
	@Test
	public void testOneToOneCreateLicense() {
		License license = new License();
		license.setType("CAR");
		license.setValidFrom(new Date());
		license.setValidTo(new Date());
		
		Person person = new Person();
		person.setFirstName("John");
		person.setLastName("Doe");
		person.setAge(35);
		
		license.setPerson(person);
		licenseRepository.save(license);
	}
 
}
