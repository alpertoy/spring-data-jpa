package io.gitlab.alpertoy.springdata.componentmapping;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.componentmapping.entities.Address;
import io.gitlab.alpertoy.springdata.componentmapping.entities.Employee;
import io.gitlab.alpertoy.springdata.componentmapping.repos.EmployeeRepository;

@SpringBootTest
class ComponentmappingApplicationTests {
	
	@Autowired
	EmployeeRepository repository;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void testCreate() {
		Employee employee = new Employee();
		
		employee.setId(123);
		employee.setName("Alper");
		
		Address address = new Address();
		
		address.setCity("Riga");
		address.setStreetAddress("Gertrudes");
		address.setCountry("Latvia");
		address.setState("Riga");
		address.setZipcode("001010");
		
		employee.setAddress(address);
		repository.save(employee);
	}

}
