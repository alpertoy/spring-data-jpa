package io.gitlab.alpertoy.springdata.files.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

/*create table image (
id BIGINT NOT NULL,
name VARCHAR(100) NOT NULL,
data BLOB NOT NULL,
PRIMARY KEY (id)
)*/

@Entity
public class Image {
	
	@Id
	private Long id;
	private String name;
	@Lob
	private byte[] data;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}
