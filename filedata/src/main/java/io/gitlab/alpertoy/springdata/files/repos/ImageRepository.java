package io.gitlab.alpertoy.springdata.files.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.files.entities.Image;

public interface ImageRepository extends CrudRepository<Image, Long> {

}
