package io.gitlab.alpertoy.springdata.hibernateinheritance.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.hibernateinheritance.entities.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {

}
