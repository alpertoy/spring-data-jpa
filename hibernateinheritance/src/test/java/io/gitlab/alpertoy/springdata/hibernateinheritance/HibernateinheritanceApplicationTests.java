package io.gitlab.alpertoy.springdata.hibernateinheritance;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.hibernateinheritance.entities.Check;
import io.gitlab.alpertoy.springdata.hibernateinheritance.entities.CreditCard;
import io.gitlab.alpertoy.springdata.hibernateinheritance.repos.PaymentRepository;

@SpringBootTest
class HibernateinheritanceApplicationTests {
	
	@Autowired
	PaymentRepository repository;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void createPayment() {
		
		CreditCard cc = new CreditCard();
		cc.setId(123);
		cc.setAmount(1000d);
		cc.setCardnumber("12344567890");
		repository.save(cc);
	}
	
	@Test
	public void createCheckPayment() {
		
		Check ch = new Check();
		ch.setId(124);
		ch.setAmount(1000d);
		ch.setChecknumber("12344567890");
		repository.save(ch);
	}

}
