package io.gitlab.alpertoy.springdata.idgenerators.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

/*create table employee(
id int,
name varchar(20)
)*/

@Entity
public class Employee {
	
	@GenericGenerator(name = "emp_id", strategy = "io.gitlab.alpertoy.springdata.idgenerators.CustomRandomIDGenerator")
	@GeneratedValue(generator = "emp_id")
	@Id
	private Long id;
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
