package io.gitlab.alpertoy.springdata.idgenerators.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.idgenerators.entities.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
