package io.gitlab.alpertoy.springdata.mongodemo.repos;

import org.springframework.data.mongodb.repository.MongoRepository;

import io.gitlab.alpertoy.springdata.mongodemo.model.Product;

public interface ProductRepository extends MongoRepository<Product, String> {

}
