package io.gitlab.alpertoy.springdata.mongodemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.mongodemo.model.Product;
import io.gitlab.alpertoy.springdata.mongodemo.repos.ProductRepository;

@SpringBootTest
class MongodemoApplicationTests {
	
	@Autowired
	ProductRepository repo;

	@Test
	public void testSave() {
		Product product = new Product();
		product.setName("Mac Book Pro");
		product.setPrice(2000f);
		Product savedProduct= repo.save(product);
		assertNotNull(savedProduct);
	}
	
	@Test
	public void testFindAll() {
		List<Product> products = repo.findAll();
		assertEquals(1,  products.size());
	}
	
	@Test
	public void testDelete() {
		repo.deleteById("5de06eda3e7beb337c905746");
		Optional<Product> product = repo.findById("5de06eda3e7beb337c905746");
		assertEquals(Optional.empty(), product);
	}

}
