package io.gitlab.alpertoy.springdata.patientscheduling.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.patientscheduling.entities.Appointment;

public interface AppointmentRepository extends CrudRepository<Appointment, Long> {

}
