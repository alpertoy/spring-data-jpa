package io.gitlab.alpertoy.springdata.patientscheduling.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.patientscheduling.entities.Doctor;

public interface DoctorRepository extends CrudRepository<Doctor, Long> {

}
