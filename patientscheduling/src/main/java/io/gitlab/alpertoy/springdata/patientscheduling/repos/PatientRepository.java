package io.gitlab.alpertoy.springdata.patientscheduling.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.patientscheduling.entities.Patient;

public interface PatientRepository extends CrudRepository<Patient, Long> {

}
