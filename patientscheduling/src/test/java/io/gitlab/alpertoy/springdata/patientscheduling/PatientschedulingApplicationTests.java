package io.gitlab.alpertoy.springdata.patientscheduling;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import io.gitlab.alpertoy.springdata.patientscheduling.entities.Appointment;
import io.gitlab.alpertoy.springdata.patientscheduling.entities.Doctor;
import io.gitlab.alpertoy.springdata.patientscheduling.entities.Insurance;
import io.gitlab.alpertoy.springdata.patientscheduling.entities.Patient;
import io.gitlab.alpertoy.springdata.patientscheduling.repos.AppointmentRepository;
import io.gitlab.alpertoy.springdata.patientscheduling.repos.DoctorRepository;
import io.gitlab.alpertoy.springdata.patientscheduling.repos.PatientRepository;

@SpringBootTest
class PatientschedulingApplicationTests {
	
	@Autowired
	DoctorRepository doctorRepository;
	
	@Autowired
	PatientRepository patientRepository;
	
	@Autowired
	AppointmentRepository appointmentRepository;
	
	@Test
	public void testCreateDoctor() {
		Doctor doctor = new Doctor();
		doctor.setFirstName("Mehmet");
		doctor.setLastName("Oz");
		doctor.setSpeciality("All");
		doctorRepository.save(doctor);
	}
	
	@Test
	public void testCreatePatient() {
		Patient patient = new Patient();
		patient.setFirstName("Alper");
		patient.setLastName("Toy");
		patient.setPhone("0123456789");
		
		Insurance insurance = new Insurance();
		insurance.setProviderName("ABC Insurance CO");
		insurance.setCopay(20d);
		
		patient.setInsurance(insurance);
		
		Doctor doctor = doctorRepository.findById(20L).get();	
		List<Doctor> doctors = Arrays.asList(doctor);
		patient.setDoctors(doctors);
		
		patientRepository.save(patient);
	}
	
	@Test
	public void testCreateAppointment() {
		
		Appointment appointment = new Appointment();
		Timestamp appointmentTime = new Timestamp(new Date().getTime());
		appointment.setAppointmentTime(appointmentTime);
		appointment.setReason("I have a problem");
		appointment.setStarted(true);
		
		appointment.setPatient(patientRepository.findById(21L).get());
		appointment.setDoctor(doctorRepository.findById(20L).get());
		
		appointmentRepository.save(appointment);
		
	}

}
