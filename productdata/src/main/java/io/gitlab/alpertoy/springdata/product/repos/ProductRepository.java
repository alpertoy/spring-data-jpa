package io.gitlab.alpertoy.springdata.product.repos;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import io.gitlab.alpertoy.springdata.product.entities.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer>{
	
	// Repository Query Keywords
	// https://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/repository-query-keywords.html
	
	List<Product> findByName(String name);
	
	List<Product> findByNameAndDesc(String name, String desc);
	
	List<Product> findByPriceGreaterThan(Double price);
	
	List<Product> findByDescContains(String desc);
	
	List<Product> findByPriceBetween(Double price1, Double price2);
	
	List<Product> findByDescLike(String desc);
	
	List<Product> findByIdIn(List<Integer> ids);
 
}
