package io.gitlab.alpertoy.springdata.transactionmanagement.repos;

import org.springframework.data.repository.CrudRepository;

import io.gitlab.alpertoy.springdata.transactionmanagement.entities.BankAccount;

public interface BankAccountRepository extends CrudRepository<BankAccount, Integer> {

}
