package io.gitlab.alpertoy.springdata.transactionmanagement.services;

public interface BankAccountService {
	
	//transferring money from alper's to john's account
	void transfer(int amount);

}
