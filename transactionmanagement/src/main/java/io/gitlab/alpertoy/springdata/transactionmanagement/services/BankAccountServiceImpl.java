package io.gitlab.alpertoy.springdata.transactionmanagement.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.gitlab.alpertoy.springdata.transactionmanagement.entities.BankAccount;
import io.gitlab.alpertoy.springdata.transactionmanagement.repos.BankAccountRepository;

@Service
public class BankAccountServiceImpl implements BankAccountService {
	
	@Autowired
	BankAccountRepository repository;
	
	//transferring money from alper's to john's account
	@Override
	@Transactional //If one of the methods crashed, all changes will be rollbacked
	public void transfer(int amount) {
		
		BankAccount alpersAccount = repository.findById(1).get();
		alpersAccount.setBal(alpersAccount.getBal() - amount);
		repository.save(alpersAccount);
		
		BankAccount johnsAccount = repository.findById(2).get();
		johnsAccount.setBal(johnsAccount.getBal() + amount);
		repository.save(johnsAccount);
	}

}
